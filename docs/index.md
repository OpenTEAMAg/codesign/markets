## Marketplace Working Group Charter

**[View the Charter](./documents/MWGCharter.pdf){:target="_blank"}**


> This Charter is a living document - to be returned to and built upon collaboratively throughout
the duration of the Marketplace Working Group. We will utilize this charter as a collective agreement and reference for the guiding purpose and process of our work. It will also serve as a documentation source to provide context for new participants who may join the working group at any point.
