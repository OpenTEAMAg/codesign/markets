## Marketplace Working Group Glossary
**[View the Glossary](./documents/MWGGlossary.pdf){:target="_blank"}**

> This glossary contains terms relevant to the Marketplace Working Group, with definitions developed by OpenTEAM. This glossary is a living document - to be returned to and built upon collaboratively throughout the duration of the Marketplace Working Group.
